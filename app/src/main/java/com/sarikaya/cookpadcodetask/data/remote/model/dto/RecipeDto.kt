package com.sarikaya.cookpadcodetask.data.remote.model.dto

import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.squareup.moshi.Json

data class RecipeDto(
    val id: Int,
    val title: String,
    val story: String,
    @Json(name = "image_url") val imageUrl: String? = null,
    @Json(name = "published_at") val publishedAt: String? = null,
    val user: UserDto? = null,
    val ingredients: List<String>? = null,
    val steps: List<RecipeStepDto>? = null
)

fun RecipeDto.toRecipe() = Recipe(
    id = id,
    title = title,
    story = story,
    imageUrl = imageUrl,
    publishedAt = publishedAt,
    user = user?.toUser(),
    ingredients = ingredients,
    steps = steps?.map { it.toRecipeStep() }
)
