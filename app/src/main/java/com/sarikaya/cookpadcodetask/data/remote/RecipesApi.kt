package com.sarikaya.cookpadcodetask.data.remote

import com.sarikaya.cookpadcodetask.data.remote.model.dto.RecipeDto
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipesApi {
    @GET("recipes")
    suspend fun getRecipeList(): List<RecipeDto>

    @GET("recipes/{id}")
    suspend fun getRecipe(@Path("id") id: String): RecipeDto
}