package com.sarikaya.cookpadcodetask.data.repository

import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.data.remote.RecipesApi
import com.sarikaya.cookpadcodetask.data.remote.model.dto.toRecipe
import com.sarikaya.cookpadcodetask.di.DispatcherModule
import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.sarikaya.cookpadcodetask.domain.repository.RecipeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class RecipeRepositoryImpl @Inject constructor(
    private val api: RecipesApi,
    @DispatcherModule.IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : RecipeRepository {
    override suspend fun getRecipes(): NetworkResult<List<Recipe>> {
        return withContext(ioDispatcher) {
            try {
                val recipes = api.getRecipeList().map {
                    it.toRecipe()
                }
                NetworkResult.Success(recipes)
            } catch (exception: HttpException) {
                NetworkResult.Error(exception.localizedMessage ?: "An error occurred")
            } catch (exception: IOException) {
                NetworkResult.Error("Network connection error occurred")
            }
        }
    }

    override suspend fun getRecipe(id: String): NetworkResult<Recipe> {
        return withContext(ioDispatcher) {
            try {
                val recipeDto = api.getRecipe(id)
                NetworkResult.Success(recipeDto.toRecipe())
            } catch (exception: HttpException) {
                NetworkResult.Error(exception.localizedMessage ?: "An error occurred")
            } catch (exception: IOException) {
                NetworkResult.Error("Network connection error occurred")
            }
        }
    }
}