package com.sarikaya.cookpadcodetask.data.remote.model.dto

import com.sarikaya.cookpadcodetask.domain.model.User
import com.squareup.moshi.Json

data class UserDto(
    val name: String,
    @Json(name = "image_url") val imageUrl: String,
)

fun UserDto.toUser() = User(
    name = name,
    imageUrl = imageUrl
)
