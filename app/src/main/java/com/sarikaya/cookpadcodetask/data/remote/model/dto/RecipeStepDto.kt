package com.sarikaya.cookpadcodetask.data.remote.model.dto

import com.sarikaya.cookpadcodetask.domain.model.RecipeStep
import com.squareup.moshi.Json

data class RecipeStepDto(
    val description: String,
    @Json(name = "image_urls") val imageUrlList: List<String>? = null
)

fun RecipeStepDto.toRecipeStep() = RecipeStep(
    description = description,
    imageUrlList = imageUrlList
)
