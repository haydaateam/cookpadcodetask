package com.sarikaya.cookpadcodetask.di

import com.sarikaya.cookpadcodetask.data.remote.RecipesApi
import com.sarikaya.cookpadcodetask.data.repository.RecipeRepositoryImpl
import com.sarikaya.cookpadcodetask.domain.repository.RecipeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideRecipesRepository(
        api: RecipesApi,
        @DispatcherModule.IoDispatcher ioDispatcher: CoroutineDispatcher
    ): RecipeRepository = RecipeRepositoryImpl(api, ioDispatcher)

}