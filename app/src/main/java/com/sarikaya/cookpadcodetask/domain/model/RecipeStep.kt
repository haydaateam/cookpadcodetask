package com.sarikaya.cookpadcodetask.domain.model

data class RecipeStep(
    val description: String,
    val imageUrlList: List<String>? = null
)