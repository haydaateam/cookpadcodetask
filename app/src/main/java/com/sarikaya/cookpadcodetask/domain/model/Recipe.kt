package com.sarikaya.cookpadcodetask.domain.model

data class Recipe(
    val id: Int,
    val title: String,
    val story: String,
    val imageUrl: String? = null,
    val publishedAt: String? = null,
    val user: User? = null,
    val ingredients: List<String>? = null,
    val steps: List<RecipeStep>? = null
)