package com.sarikaya.cookpadcodetask.domain.model

data class User(
    val name: String,
    val imageUrl: String
)