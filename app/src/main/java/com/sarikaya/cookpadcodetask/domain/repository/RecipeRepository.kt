package com.sarikaya.cookpadcodetask.domain.repository

import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.domain.model.Recipe

interface RecipeRepository {
    suspend fun getRecipes(): NetworkResult<List<Recipe>>

    suspend fun getRecipe(id:String): NetworkResult<Recipe>
}