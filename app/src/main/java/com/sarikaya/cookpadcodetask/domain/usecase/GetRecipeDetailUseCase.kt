package com.sarikaya.cookpadcodetask.domain.usecase

import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.sarikaya.cookpadcodetask.domain.repository.RecipeRepository
import javax.inject.Inject

class GetRecipeDetailUseCase @Inject constructor(
    private val repository: RecipeRepository
) {
    suspend operator fun invoke(id: String): NetworkResult<Recipe> =
        repository.getRecipe(id)
}