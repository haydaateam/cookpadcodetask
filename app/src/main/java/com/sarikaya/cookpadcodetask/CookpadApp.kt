package com.sarikaya.cookpadcodetask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CookpadApp : Application() {
}