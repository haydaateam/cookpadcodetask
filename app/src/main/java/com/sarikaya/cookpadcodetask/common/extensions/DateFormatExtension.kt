package com.sarikaya.cookpadcodetask.common.extensions

import java.text.SimpleDateFormat
import java.util.*

private const val OUTPUT_DATE_PATTERN = "dd.MM.yyyy"
private const val INPUT_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"

fun String.convertISOTimeToDate(): String? {
    val format = SimpleDateFormat(OUTPUT_DATE_PATTERN, Locale.US)
    val date = SimpleDateFormat(
        INPUT_DATE_PATTERN,
        Locale.US
    ).parse(this.split("+")[0])
    return date?.let { format.format(it) }
}