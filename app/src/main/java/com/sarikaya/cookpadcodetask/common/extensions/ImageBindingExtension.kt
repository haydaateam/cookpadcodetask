package com.sarikaya.cookpadcodetask.common.extensions

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.sarikaya.cookpadcodetask.R

@BindingAdapter("app:imageUrl")
fun AppCompatImageView.setImageUrl(url: String?) {
    Glide.with(this).load(url)
        .placeholder(R.drawable.ic_image_default)
        .into(this)
}