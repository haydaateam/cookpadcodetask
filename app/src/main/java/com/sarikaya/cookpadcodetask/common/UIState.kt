package com.sarikaya.cookpadcodetask.common

enum class UIState {
    IDLE,
    LOADING,
    ERROR,
    CONTENT
}