package com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeInfoBinding
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.RecipeDetailListAdapter
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.entity.RecipeDetailItem

class RecipeInfoViewHolder(private val binding: ItemRecipeInfoBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: RecipeDetailItem.InfoItem) {
        binding.data = item
        binding.rvIngredients.run {
            layoutManager = GridLayoutManager(itemView.context, 2)
            setHasFixedSize(true)
            adapter = RecipeDetailListAdapter(item.ingredients)
        }
        binding.executePendingBindings()
    }
}