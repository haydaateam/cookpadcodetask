package com.sarikaya.cookpadcodetask.presentation.recipelist.adapter

import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeListBinding
import com.sarikaya.cookpadcodetask.domain.model.Recipe

class RecipeViewHolder(
    private val binding: ItemRecipeListBinding,
    private val onClickedItem: ((Int?) -> Unit)? = null
) : RecyclerView.ViewHolder(binding.root) {

    init {
        itemView.setOnClickListener {
            onClickedItem?.invoke(adapterPosition)
        }
    }

    fun bind(recipe: Recipe?) {
        recipe?.let {
            binding.itemData = it
            binding.executePendingBindings()
        }

    }
}