package com.sarikaya.cookpadcodetask.presentation.navigation

object Args {
    const val ARG_RECIPE_ID = "recipeId"
}