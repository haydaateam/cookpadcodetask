package com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeInfoBinding
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeIngredientsBinding
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeStepBinding
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.entity.RecipeDetailItem
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder.RecipeInfoViewHolder
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder.RecipeIngredientItemViewHolder
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder.RecipeStepItemViewHolder

class RecipeDetailListAdapter(
    private val listOfRecipeDetail: List<RecipeDetailItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_INFO_ITEM = 1
        private const val TYPE_STEP_ITEM = 2
        private const val TYPE_INGREDIENT_ITEM = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_INFO_ITEM -> RecipeInfoViewHolder(
                ItemRecipeInfoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            TYPE_STEP_ITEM -> RecipeStepItemViewHolder(
                ItemRecipeStepBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            else -> RecipeIngredientItemViewHolder(
                ItemRecipeIngredientsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RecipeStepItemViewHolder -> holder.bind(listOfRecipeDetail[position] as RecipeDetailItem.StepsItem)
            is RecipeInfoViewHolder -> holder.bind(listOfRecipeDetail[position] as RecipeDetailItem.InfoItem)
            is RecipeIngredientItemViewHolder -> holder.bind((listOfRecipeDetail[position] as RecipeDetailItem.IngredientItem).text)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (listOfRecipeDetail[position]) {
            is RecipeDetailItem.InfoItem -> TYPE_INFO_ITEM
            is RecipeDetailItem.StepsItem -> TYPE_STEP_ITEM
            is RecipeDetailItem.IngredientItem -> TYPE_INGREDIENT_ITEM
        }
    }

    override fun getItemCount() = listOfRecipeDetail.size

}