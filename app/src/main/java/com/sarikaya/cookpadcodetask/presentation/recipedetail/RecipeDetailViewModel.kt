package com.sarikaya.cookpadcodetask.presentation.recipedetail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.domain.usecase.GetRecipeDetailUseCase
import com.sarikaya.cookpadcodetask.presentation.navigation.Args
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecipeDetailViewModel @Inject constructor(
    private val getRecipeDetailUseCase: GetRecipeDetailUseCase,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _state = MutableStateFlow(RecipeDetailState())
    internal val state: StateFlow<RecipeDetailState> = _state

    init {
        getRecipe()
    }

    internal fun getRecipe() {
        val recipeId = savedStateHandle.get<String>(Args.ARG_RECIPE_ID)

        if (recipeId == null) {
            _state.value = RecipeDetailState(
                uiState = UIState.ERROR
            )
            return
        }

        viewModelScope.launch {
            _state.value = RecipeDetailState(
                UIState.LOADING
            )
            val result = getRecipeDetailUseCase(recipeId)
            _state.value = when (result) {
                is NetworkResult.Success -> RecipeDetailState(
                    UIState.CONTENT,
                    result.data
                )
                is NetworkResult.Error -> RecipeDetailState(
                    UIState.ERROR,
                    message = result.message
                )
            }
        }
    }
}