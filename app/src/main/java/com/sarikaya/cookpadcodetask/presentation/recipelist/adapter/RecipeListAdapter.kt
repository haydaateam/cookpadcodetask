package com.sarikaya.cookpadcodetask.presentation.recipelist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeListBinding
import com.sarikaya.cookpadcodetask.domain.model.Recipe

class RecipeListAdapter(
    private val onClickedItem: ((Int?) -> Unit)? = null
) : RecyclerView.Adapter<RecipeViewHolder>() {
    private var recipes: List<Recipe>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        return RecipeViewHolder(
            ItemRecipeListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ) {
            it?.let { position ->
                onClickedItem?.invoke(recipes?.get(position)?.id)
            }
        }
    }

    fun submitList(recipeList: List<Recipe>?) {
        recipes = recipeList
        notifyItemRangeChanged(0, recipes?.size ?: 0)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bind(recipes?.get(position))
    }

    override fun getItemCount() = recipes?.size ?: 0

}