package com.sarikaya.cookpadcodetask.presentation.recipedetail

import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.domain.model.Recipe

data class RecipeDetailState(
    val uiState: UIState = UIState.IDLE,
    val recipe: Recipe? = null,
    val message: String? = null,
)
