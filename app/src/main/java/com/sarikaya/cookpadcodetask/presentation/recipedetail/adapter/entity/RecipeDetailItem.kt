package com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.entity

import com.sarikaya.cookpadcodetask.domain.model.RecipeStep
import com.sarikaya.cookpadcodetask.domain.model.User

sealed class RecipeDetailItem {
    data class InfoItem(
        val title: String,
        val story: String,
        val publishedDate: String,
        val user: User?,
        val ingredients: List<IngredientItem>
    ) : RecipeDetailItem()

    data class StepsItem(
        val step: RecipeStep
    ) : RecipeDetailItem()

    data class IngredientItem(val text: String) : RecipeDetailItem()
}
