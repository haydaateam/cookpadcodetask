package com.sarikaya.cookpadcodetask.presentation.recipedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.common.extensions.convertISOTimeToDate
import com.sarikaya.cookpadcodetask.common.extensions.setImageUrl
import com.sarikaya.cookpadcodetask.databinding.FragmentRecipeDetailBinding
import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.RecipeDetailListAdapter
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.entity.RecipeDetailItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RecipeDetailFragment : Fragment() {
    lateinit var binding: FragmentRecipeDetailBinding

    private val viewModel: RecipeDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.statusBarColor = ContextCompat.getColor(requireContext(), android.R.color.transparent)
        lifecycleScope.launch {
            viewModel.state.collect {
                hideLoading()
                when (it.uiState) {
                    UIState.LOADING -> {
                        showLoading()
                        binding.errorView.visibility = View.GONE
                    }
                    UIState.CONTENT -> {
                        binding.rvDetailList.run {
                            it.recipe?.let { recipeDetail ->
                                binding.ivRecipeImage.setImageUrl(recipeDetail.imageUrl)
                                layoutManager = LinearLayoutManager(context)
                                setHasFixedSize(true)
                                adapter = RecipeDetailListAdapter(getDetailList(recipeDetail))
                            }
                        }
                    }
                    UIState.ERROR -> {
                        binding.errorView.apply {
                            visibility = View.VISIBLE
                            text = it.message.toString()
                            onClick = { viewModel.getRecipe() }
                        }
                    }
                    UIState.IDLE -> {
                        // do nothing
                    }
                }
            }
        }
    }

    private fun getDetailList(recipeDetail: Recipe): List<RecipeDetailItem> {
        val listOfRecipeDetail = mutableListOf<RecipeDetailItem>()
        listOfRecipeDetail.add(RecipeDetailItem.InfoItem(
            recipeDetail.title,
            recipeDetail.story,
            recipeDetail.publishedAt?.convertISOTimeToDate().toString(),
            recipeDetail.user,
            recipeDetail.ingredients?.map { ingredient ->
                RecipeDetailItem.IngredientItem(ingredient)
            }!!
        ))
        recipeDetail.steps?.forEach { step ->
            listOfRecipeDetail.add(
                RecipeDetailItem.StepsItem(
                    step
                )
            )
        }
        return listOfRecipeDetail
    }

    private fun showLoading(){
        binding.clpLoading.visibility = View.VISIBLE
    }

    private fun hideLoading(){
        binding.clpLoading.visibility = View.GONE
    }
}