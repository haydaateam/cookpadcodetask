package com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeIngredientsBinding

class RecipeIngredientItemViewHolder(private val binding: ItemRecipeIngredientsBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: String) {
        binding.text = item
        binding.executePendingBindings()
    }
}