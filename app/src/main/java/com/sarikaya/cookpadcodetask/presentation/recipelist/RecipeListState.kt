package com.sarikaya.cookpadcodetask.presentation.recipelist

import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.domain.model.Recipe

data class RecipeListState(
    val uiState: UIState = UIState.IDLE,
    val recipeList: List<Recipe>? = null,
    val errorMessage: String? = null
)
