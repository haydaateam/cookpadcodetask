package com.sarikaya.cookpadcodetask.presentation.components

import android.content.Context
import android.util.AttributeSet
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.AbstractComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.sarikaya.cookpadcodetask.R

@Composable
fun ErrorComposableView(
    message: String? = null,
    onItemClick: () -> Unit
) {
    // Column
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = CenterHorizontally
    ) {
        Button(
            onClick = { onItemClick() }, colors = ButtonDefaults.textButtonColors(
                backgroundColor = colorResource(id = R.color.primaryDarkColor)
            )
        ) {
            message?.let {
                Text(
                    stringResource(id = R.string.error_pattern, it),
                    color = colorResource(id = R.color.white),
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}

class ErrorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AbstractComposeView(context, attrs, defStyle) {

    var text by mutableStateOf("")
    var onClick by mutableStateOf({})

    @Composable
    override fun Content() {
        MaterialTheme {
            ErrorComposableView(text, onClick)
        }
    }
}