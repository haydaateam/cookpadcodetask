package com.sarikaya.cookpadcodetask.presentation.recipelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.sarikaya.cookpadcodetask.R
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.databinding.FragmentRecipeListBinding
import com.sarikaya.cookpadcodetask.presentation.recipelist.adapter.RecipeListAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RecipeListFragment : Fragment() {

    private val viewModel: RecipeListViewModel by viewModels()

    lateinit var binding: FragmentRecipeListBinding

    private val listAdapter: RecipeListAdapter by lazy {
        RecipeListAdapter {
            Navigation.findNavController(requireView())
                .navigate(
                    RecipeListFragmentDirections.actionRecipeListFragmentToRecipeDetailFragment(
                        it.toString()
                    )
                )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.primaryDarkColor)
        lifecycleScope.launch {
            viewModel.state.collect {
                hideLoading()
                when (it.uiState) {
                    UIState.CONTENT -> {
                        binding.rvRecipeList.run {
                            layoutManager = GridLayoutManager(context, 2)
                            setHasFixedSize(true)
                            adapter = listAdapter
                        }
                        listAdapter.submitList(it.recipeList)
                    }
                    UIState.ERROR -> {
                        binding.errorView.apply {
                            visibility = View.VISIBLE
                            text = it.errorMessage.toString()
                            onClick = { viewModel.getRecipeList() }
                        }
                    }
                    UIState.LOADING -> {
                        showLoading()
                        binding.errorView.visibility = View.GONE
                    }
                    UIState.IDLE -> {
                        //do nothing
                    }
                }
            }
        }
    }

    private fun showLoading() {
        binding.clpLoading.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.clpLoading.visibility = View.GONE
    }
}