package com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.sarikaya.cookpadcodetask.databinding.ItemRecipeStepBinding
import com.sarikaya.cookpadcodetask.presentation.recipedetail.adapter.entity.RecipeDetailItem

class RecipeStepItemViewHolder(private val binding: ItemRecipeStepBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: RecipeDetailItem.StepsItem) {
        binding.data = item
        binding.executePendingBindings()
    }
}