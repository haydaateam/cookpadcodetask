package com.sarikaya.cookpadcodetask.presentation.recipelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.domain.usecase.GetRecipesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecipeListViewModel @Inject constructor(
    private val getRecipesUseCase: GetRecipesUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(RecipeListState())
    internal val state: StateFlow<RecipeListState> = _state

    init {
        getRecipeList()
    }

    internal fun getRecipeList() {
        _state.value = RecipeListState(
            uiState = UIState.LOADING
        )

        viewModelScope.launch {
            val result = getRecipesUseCase()

            _state.value = when (result) {
                is NetworkResult.Success -> RecipeListState(
                    uiState = UIState.CONTENT,
                    recipeList = result.data
                )
                is NetworkResult.Error -> RecipeListState(
                    uiState = UIState.ERROR,
                    errorMessage = result.message
                )
            }
        }
    }
}