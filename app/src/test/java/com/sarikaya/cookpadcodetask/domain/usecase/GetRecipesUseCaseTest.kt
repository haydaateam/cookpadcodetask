package com.sarikaya.cookpadcodetask.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.core.MockRecipeProvider
import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.sarikaya.cookpadcodetask.domain.repository.RecipeRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class GetRecipesUseCaseTest {

    lateinit var useCase: GetRecipesUseCase

    private val recipesRepository: RecipeRepository = mock()

    @Before
    fun setup() {
        useCase = GetRecipesUseCase(recipesRepository)
    }

    @Test
    fun `fetch recipes from repository`() = runBlocking {
        val mockRecipes = MockRecipeProvider.getMockRecipes()
        val expectedResult = NetworkResult.Success(data = mockRecipes)
        whenever(recipesRepository.getRecipes())
            .thenReturn(expectedResult as NetworkResult<List<Recipe>>)

        val resource = useCase()

        verify(recipesRepository).getRecipes()
        assertThat(resource).isEqualTo(expectedResult)
    }
}