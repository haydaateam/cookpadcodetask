package com.sarikaya.cookpadcodetask.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.core.MockRecipeProvider
import com.sarikaya.cookpadcodetask.domain.model.Recipe
import com.sarikaya.cookpadcodetask.domain.repository.RecipeRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class GetRecipeDetailUseCaseTest {

    lateinit var useCase: GetRecipeDetailUseCase

    private val recipesRepository: RecipeRepository = mock()

    @Before
    fun setup() {
        useCase = GetRecipeDetailUseCase(recipesRepository)
    }

    @Test
    fun `fetch recipe detail from repository`() = runBlocking {
        val id = 1
        val mockRecipe = MockRecipeProvider.getMockRecipe(id)
        val expectedResult = NetworkResult.Success(data = mockRecipe)
        whenever(recipesRepository.getRecipe(id.toString()))
            .thenReturn(expectedResult as NetworkResult<Recipe>)

        val resource = useCase(id.toString())

        verify(recipesRepository).getRecipe(id.toString())
        assertThat(resource).isEqualTo(expectedResult)
    }
}