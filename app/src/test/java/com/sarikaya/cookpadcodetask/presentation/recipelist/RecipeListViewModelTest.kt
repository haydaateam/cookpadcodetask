package com.sarikaya.cookpadcodetask.presentation.recipelist

import com.google.common.truth.Truth
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.core.MockRecipeProvider
import com.sarikaya.cookpadcodetask.domain.usecase.GetRecipesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class RecipeListViewModelTest {
    private lateinit var viewModel: RecipeListViewModel

    private val getRecipeListUseCase: GetRecipesUseCase = mock()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @Test
    fun `fetch data successfully and update state`() = runTest {
        val mockResponse = MockRecipeProvider.getMockRecipes()

        whenever(getRecipeListUseCase())
            .thenReturn(NetworkResult.Success(data = mockResponse))

        viewModel = RecipeListViewModel(getRecipeListUseCase)

        Truth.assertThat(viewModel.state.value).isEqualTo(
            RecipeListState(
                uiState = UIState.CONTENT,
                recipeList = mockResponse
            )
        )
    }

    @Test
    fun `fetch data with exception and update state`() = runTest {
        whenever(getRecipeListUseCase())
            .thenReturn(NetworkResult.Error(message = "Error"))

        viewModel = RecipeListViewModel(getRecipeListUseCase)

        Truth.assertThat(viewModel.state.value).isEqualTo(
            RecipeListState(
                uiState = UIState.ERROR,
                recipeList = null,
                errorMessage = "Error"
            )
        )
    }
}