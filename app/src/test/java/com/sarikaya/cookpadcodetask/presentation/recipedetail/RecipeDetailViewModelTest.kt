package com.sarikaya.cookpadcodetask.presentation.recipedetail

import androidx.lifecycle.SavedStateHandle
import com.google.common.truth.Truth
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.common.UIState
import com.sarikaya.cookpadcodetask.core.MockRecipeProvider
import com.sarikaya.cookpadcodetask.domain.usecase.GetRecipeDetailUseCase
import com.sarikaya.cookpadcodetask.presentation.navigation.Args
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class RecipeDetailViewModelTest {
    private lateinit var viewModel: RecipeDetailViewModel

    private val getRecipeDetailUseCase: GetRecipeDetailUseCase = mock()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @Test
    fun `error state in case recipe id not exists`() {
        val recipeId = null
        val savedStateHandle = SavedStateHandle().apply {
            set(Args.ARG_RECIPE_ID, recipeId)
        }

        viewModel = RecipeDetailViewModel(getRecipeDetailUseCase, savedStateHandle)

        Truth.assertThat(viewModel.state.value).isEqualTo(RecipeDetailState(uiState = UIState.ERROR))
    }

    @Test
    fun `fetch data successfully and update state`() = runTest {
        val recipeId = "1"
        val savedStateHandle = SavedStateHandle().apply {
            set(Args.ARG_RECIPE_ID, recipeId)
        }

        val mockResponse = MockRecipeProvider.getMockRecipe(recipeId.toInt())

        whenever(getRecipeDetailUseCase(recipeId))
            .thenReturn(NetworkResult.Success(data = mockResponse))

        viewModel = RecipeDetailViewModel(getRecipeDetailUseCase, savedStateHandle)

        Truth.assertThat(viewModel.state.value).isEqualTo(
            RecipeDetailState(
                uiState = UIState.CONTENT,
                recipe = mockResponse
            )
        )
    }

    @Test
    fun `fetch data with exception and update state`() = runTest {
        val recipeId = "1"
        val savedStateHandle = SavedStateHandle().apply {
            set(Args.ARG_RECIPE_ID, recipeId)
        }
        whenever(getRecipeDetailUseCase(recipeId))
            .thenReturn(NetworkResult.Error(message = "Error"))

        viewModel = RecipeDetailViewModel(getRecipeDetailUseCase, savedStateHandle)

        Truth.assertThat(viewModel.state.value).isEqualTo(
            RecipeDetailState(
                uiState = UIState.ERROR,
                recipe = null,
                message = "Error"
            )
        )
    }
}