package com.sarikaya.cookpadcodetask.data.repository

import com.google.common.truth.Truth.assertThat
import com.sarikaya.cookpadcodetask.common.NetworkResult
import com.sarikaya.cookpadcodetask.core.MockRecipesNetworkApiConfigurationTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import java.net.HttpURLConnection

@ExperimentalCoroutinesApi
class RecipeRepositoryImplTest : MockRecipesNetworkApiConfigurationTest() {

    private lateinit var repository: RecipeRepositoryImpl

    override fun setup() {
        super.setup()
        repository = RecipeRepositoryImpl(recipesApi, dispatcher)
    }

    @Test
    fun `fetch recipes successfully by given mock data`() = runTest {
        mockHttpResponse("api_response/mock_recipe_list.json", HttpURLConnection.HTTP_OK)

        val result = repository.getRecipes()
        assertThat(result).isInstanceOf(NetworkResult.Success::class.java)
        assertThat(result.data).isNotNull()
    }

    @Test
    fun `fetch recipes with exception`() = runTest {
        mockHttpResponse("api_response/mock_recipe_list.json", HttpURLConnection.HTTP_FORBIDDEN)

        val result = repository.getRecipes()
        assertThat(result).isInstanceOf(NetworkResult.Error::class.java)
        assertThat(result.data).isNull()
    }

    @Test
    fun `fetch recipe detail successfully by given mock data`() = runTest {
        val id = "1"
        mockHttpResponse("api_response/mock_recipe.json", HttpURLConnection.HTTP_OK)

        val result = repository.getRecipe(id)
        assertThat(result).isInstanceOf(NetworkResult.Success::class.java)
        assertThat(result.data).isNotNull()
    }

    @Test
    fun `fetch recipe detail with exception`() = runTest {
        val id = "1"
        mockHttpResponse("api_response/mock_recipe.json", HttpURLConnection.HTTP_FORBIDDEN)

        val result = repository.getRecipe(id)
        assertThat(result).isInstanceOf(NetworkResult.Error::class.java)
        assertThat(result.data).isNull()
    }
}