package com.sarikaya.cookpadcodetask.core

import com.sarikaya.cookpadcodetask.domain.model.Recipe

object MockRecipeProvider {
    fun getMockRecipe(id: Int = 2380570) =
        Recipe(
            id = id,
            title = "Lorem Ipsum Sit Amet",
            story = "Lorem Impsum Sit Amet"
        )

    fun getMockRecipes() =
        (1..6).map {
            getMockRecipe(id = it)
        }
}