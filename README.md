<h2> Sample App Screenshots</h2>
Recipe List App to apply clean-architecture on Android project

<img src="https://bitbucket.org/haydaateam/cookpadcodetask/src/master/app/src/main/assets/images/error_screen.png" alt="Error Screen" width="270"/>&nbsp;
<img src="https://bitbucket.org/haydaateam/cookpadcodetask/src/master/app/src/main/assets/images/recipe_list_screen.png" alt="List Screen" width="270"/>&nbsp;
<img src="https://bitbucket.org/haydaateam/cookpadcodetask/src/master/app/src/main/assets/images/recipe_detail_screen.png" alt="Detail Screen" width="270"/>

<h2>Architecture</h2>
Clean Architecture principles are applied with MVVM pattern

<h2>Tech-stack</h2>
Main dependencies used in the project:

- Retrofit
- Moshi
- Coroutines
- Dagger-Hilt
- Navigation
- Compose (has been adopted into this project as interoperable for error view)

<h2>Approach of project</h2>
<p>
I have used api services of recipe in order to display list and detail of them.
In UI side, I have also created the screens by using RecyclerView to use benefits of it such as layout managers and animations.
I have also tried to adopt compose library into this project as interoperable. I have created and implemented errorview by using compose.
For navigating between screens, I have used navigation component instead of creating fragment transaction.
As architectural, I have created clean-architecture layers to decouple responsibilities of my codes.
While I was handling IO operations, I have also used coroutines instead of RxJava in order to create concurrent codes.
Because RxJava streams are prone to leaks, where a stream continues to process items even when we no longer care.
And also there are a lot of streaming types in RxJava but using of suspend fun and flow type in coroutine is enough unlike RxJava.
</p>

<h2>Future</h2>
<p>
If I had more time, I would write UI test with espresso by learning it and could improve my UI/UX or fix placing of views issues on layouts.
And I could also create my list and detail screens by using compose. Because it was my first implementation for compose.
It could be risk for me in this restricted time.
</p>

<h2>Environment Requirements</h2>
<p>
You must download the latest version of Android Studio (Arctic Fox) and Gradle plugin in order to setup and run this project.
Kotlin version should be 1.6.10 to support current compose version. Because I am using compose with normal project together.
Device API version should be more than 21.
</p>